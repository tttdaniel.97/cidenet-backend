package com.cidenet.controller;

import com.cidenet.entity.EmployeeEntity;
import com.cidenet.service.EmployeeService;
import com.cidenet.util.exception.ApiRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/cidenet")
@CrossOrigin(origins = "*")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping(value = "/employees")
    public List<EmployeeEntity> getEmployees() {
        //throw new ApiRequestException("Un error por aqui");
        return employeeService.getEmployees();

    }

    @PostMapping(value = "/employee")
    public ResponseEntity createEmployee(@RequestBody EmployeeEntity employee) {

        return employeeService.createEmployee(employee);

    }

    @PutMapping(value = "/employee")
    public ResponseEntity updateEmployee(@RequestBody EmployeeEntity employee) {

        return employeeService.updateEmployee(employee);

    }

    @DeleteMapping(value = "/employee/{idEmployee}")
    public ResponseEntity deleteEmployee(@PathVariable("idEmployee") String idEmployee) {

        return employeeService.deleteEmployee(idEmployee);

    }

}
