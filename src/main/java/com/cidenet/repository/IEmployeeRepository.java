package com.cidenet.repository;

import com.cidenet.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IEmployeeRepository extends JpaRepository<EmployeeEntity, Integer> {

    Boolean existsByIdEmployeeAndTypeId(String idEmployee, Integer typeId);
    Boolean existsByEmail(String email);
    EmployeeEntity findByIdEmployee(String idemployee);
    void deleteByIdEmployee(String idEmployee);

}
