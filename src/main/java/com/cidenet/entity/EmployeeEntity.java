package com.cidenet.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "employees")
public class EmployeeEntity {

    @Id
    @Column(name = "id_employee", unique = true)
    private String idEmployee;

    @Column(name = "frist_surname")
    private String fristSurname;

    @Column(name = "second_surname")
    private String secondSurname;

    private String name;

    @Column(name = "other_names")
    private String otherNames;

    @Column(name = "id_type")
    private Integer typeId;

    @Column(name = "employments_country")
    private String employmentsCountry;

    @Column(unique = true)
    private String email;

    @OneToOne()
    @JoinColumn(name = "id_area", referencedColumnName = "id_area")
    private Area area;

    private String status;

    @Column(name = "registration_date")
    private Timestamp registrationDate;

    @Column(name = "edition_date")
    private Timestamp editionDate;

}
