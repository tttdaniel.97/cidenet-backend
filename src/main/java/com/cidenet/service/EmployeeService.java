package com.cidenet.service;

import com.cidenet.entity.EmployeeEntity;
import com.cidenet.repository.IEmployeeRepository;
import com.cidenet.util.exception.ApiException;
import com.cidenet.util.exception.ApiRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


@Service
public class EmployeeService {

    Logger logger = LoggerFactory.getLogger(EmployeeService.class);
    @Autowired
    private IEmployeeRepository employeeRepository;

    public List<EmployeeEntity> getEmployees() {
        return employeeRepository.findAll();
    }

    @Transactional
    public ResponseEntity createEmployee(EmployeeEntity employeeEntity) throws RuntimeException {

        try {

            if (validIdEmployee(employeeEntity.getIdEmployee(), employeeEntity.getTypeId())) {
                throw new ApiRequestException("La identificación del empleado ya existe");
            }
            employeeEntity.setEmail(buildEmail(employeeEntity, 0));
            employeeRepository.save(employeeEntity);
            return new ResponseEntity(employeeEntity, HttpStatus.CREATED);

        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }

    }

    @Transactional
    public ResponseEntity updateEmployee(EmployeeEntity employeeEntity) throws RuntimeException {

        try {

            EmployeeEntity oldEmployee = employeeRepository.findByIdEmployee(employeeEntity.getIdEmployee());
            if (oldEmployee.getName() != employeeEntity.getName() || oldEmployee.getFristSurname() != employeeEntity.getFristSurname()) {
                employeeEntity.setEmail(buildEmail(employeeEntity, 0));
            }
            employeeRepository.save(employeeEntity);
            return new ResponseEntity(employeeEntity, HttpStatus.OK);

        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }

    }

    @Transactional
    public ResponseEntity deleteEmployee(String idEmployee) throws RuntimeException {

        try {
            employeeRepository.deleteByIdEmployee(idEmployee);
            return new ResponseEntity(new EmployeeEntity(), HttpStatus.OK);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }

    }

    private boolean validIdEmployee(String idEmployee, Integer typeId) {
        return employeeRepository.existsByIdEmployeeAndTypeId(idEmployee, typeId);
    }

    private String buildEmail(EmployeeEntity employee, Integer idEmail) {

        String id = idEmail == 0 ? "" : idEmail.toString();
        String domain = employee.getEmploymentsCountry().equals("Colombia") ? "cidenet.com.co" : "cidenet.com.us";
        String newEmail = employee.getName().toLowerCase(Locale.ROOT) + "." +
                          employee.getFristSurname().toLowerCase().replaceAll("\\s+","") +
                          id + "@" + domain;

        if (employeeRepository.existsByEmail(newEmail)) {
            idEmail += 1;
            return buildEmail(employee, idEmail);
        } else {
            return newEmail;
        }

    }

}
