package com.cidenet.service;

import com.cidenet.entity.Area;
import com.cidenet.entity.EmployeeEntity;
import com.cidenet.repository.IEmployeeRepository;
import com.cidenet.util.exception.ApiException;
import com.cidenet.util.exception.ApiExceptionHandler;
import com.cidenet.util.exception.ApiRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
class EmployeeServiceTest {

    @Mock
    private IEmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeService employeeService;

    @Autowired
    private IEmployeeRepository employeeRepositoryInject;

    private EmployeeEntity employee;
    private Area area;

    @BeforeEach
    void setUp() {
        area = Area.builder()
                .id(1)
                .description("Administración")
                .build();

        employee = EmployeeEntity.builder()
                .fristSurname("Arboleda")
                .secondSurname("Tamayo")
                .idEmployee("1234f")
                .name("Daniel")
                .status("Activo")
                .typeId(1)
                .registrationDate(new Timestamp(new Date().getTime()))
                .employmentsCountry("Colombia")
                .build();

    }

    @DisplayName("The findAll method is executing correctly")
    @Test
    void canFindAllEmployees() {
        employeeService.getEmployees();
        verify(employeeRepository).findAll();
    }

    @DisplayName("The delete method is executing correctly")
    @Test
    void canDeleteEmployees() {
        employeeService.deleteEmployee(employee.getIdEmployee());
        verify(employeeRepository).deleteByIdEmployee(employee.getIdEmployee());
    }

    @DisplayName("Ensure that all saved employees are returning correctly")
    @Test
    void whenEmployeesAreReturningCorrectly() {

        EmployeeEntity secondEmployee = EmployeeEntity.builder()
                .fristSurname("Arboleda")
                .secondSurname("Tamayo")
                .idEmployee("1234ff")
                .name("Daniel2")
                .status("Activo")
                .typeId(1)
                .registrationDate(new Timestamp(new Date().getTime()))
                .employmentsCountry("Colombia")
                .build();

        employeeRepositoryInject.save(employee);
        employeeRepositoryInject.save(secondEmployee);

        List<EmployeeEntity> employeeList = employeeRepositoryInject.findAll();

        assertThat(employeeList).isNotNull();
        assertThat(employeeList.size()).isEqualTo(2);
    }

    @DisplayName("The idEmployee validator is working correctly")
    @Test
    void itShouldThrowErrorWhenIdDuplicate() {
        EmployeeEntity duplicatedEmployee = EmployeeEntity.builder()
                .fristSurname("Arboleda")
                .secondSurname("Tamayo")
                .idEmployee("1234f")
                .name("Daniel")
                .status("Activo")
                .typeId(1)
                .registrationDate(new Timestamp(new Date().getTime()))
                .employmentsCountry("Colombia")
                .build();

        employeeRepositoryInject.save(employee);
        assertThat(employeeRepository.existsByIdEmployeeAndTypeId(duplicatedEmployee.getIdEmployee(),
                duplicatedEmployee.getTypeId())).isTrue();
    }

}